# Installation instructions for the new vue.js based UIkitty 3 website.

It is important that you already have node and npm installed. Additionally the site will not build correctly unless you have installed globally the vue template compiler.
`npm install -g vue-template-compiler`

UIkitty 3 website

Run ```npm install && npm run setup``` after cloning
